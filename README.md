# Запуск кластера и деплой приложения.
В рамках данного задания будем использовать наработки из лабораторной работы №2.

```bash
brew install minikube
minikube start

minikube dashboard

minikube addons enable registry
docker run --rm -it --network=host alpine ash -c "apk add socat && socat TCP-LISTEN:5000,reuseaddr,fork TCP:$(minikube ip):5000"

docker build -t localhost:5000/mytestimage .
docker push localhost:5000/mytestimage

kubectl apply -f ./k8s/namesapce.yml
kubectl apply -f ./k8s/dep.yml & kubectl apply -f ./k8s/serv.yml

minikube tunnel
```

# Устанавливаем  Prometheus и Grafana

## Установка helm
Для установки всех компонентов удобно использовать helm charts. Поэтому установим его.
```bash
brew install helm
```

## Установка Prometheus
```bash
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm install prometheus prometheus-community/prometheus
```


## Установка Grafana
```bash
helm repo add grafana https://grafana.github.io/helm-charts
helm install grafana grafana/grafana
```

## Проверка сервисов
Для начала работы необходимо пробросить порты для веб-интерфеса графаны и прометеуса.

```bash
kubectl expose service prometheus-server --type=NodePort --target-port=9090 --name=prometheus-server-np
kubectl expose service grafana --type=NodePort --target-port=3000 --name=grafana-np
```

Для проверки серивса Prometheus получаем его URL
```bash
minikube service prometheus-server-np --url
```

Для проверки серивса Grafana получаем его URL
```bash
minikube service grafana-np --url
```

Для того, чтобы использовать Grafana необходимо авторизироваться, для этого необходимо получить пароль
```bash
kubectl get secret --namespace default grafana -o jsonpath="{.data.admin-password}" | base64 --decode
```

![prom](docs/prom.png)
![grafana](docs/grafana.png)


## Вывод дашбордов Grafana
Для того чтобы вывести дашборд в графане, нужно указать в качестве датасурса prometheus. 

![set_up_prom](docs/set_up_prom.png)


## Создаём дашборд
![dashboard](docs/dashboard.png)

# Задание со звёздочкой
## Создаём бота
![bot_create](docs/bot_create.png)


## Полчаем идентификатор чата
![chat](docs/chat.png)

## Создаём манифесты для alertmanager
![k8s_configmap](docs/k8s_configmap.png)
![k8s_deployment](docs/k8s_deployment.png)


## Разворачиваем сервис alertmanager
```bash
kubectl apply -f ./k8s/alrtm
```


## Создадим тестовые уведомления и проверим их работоспособность
```bash
amtool --alertmanager.url=http://localhost:9093/ alert add alertname="test123" severity="test-telegram" job="test-alert" instance="localhost" exporter="none" cluster="test"
```

## Проверяем сообщение в Telegram
![alert](docs/alert.png)


